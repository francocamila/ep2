package views;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.GroupLayout;

import control.Control;
import interfaces.Observer;


public class Game extends javax.swing.JFrame implements Observer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 320;
	public static final int HEIGHT = WIDTH /12 * 9;
	public static final int SCALE = 2;
	
	public final String TITLE = "Snake Game";
	
	//private boolean running = false;
    //private Thread thread;
    
    public Game(Control c) {
		initComponents();
        control = c;		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();
		setSize(width,height);
		
		//JFrame frame = new JFrame(TITLE);
		//frame.add(game);
		//frame.pack();
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setResizable(false);
		//frame.setLocale(null);
		//frame.setLocationRelativeTo(null);
		//frame.setVisible(true);
		setResizable(false);
		setTitle("sBake Game");
		startGraphicGame();
	}
	
	//private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	//private synchronized void start() {
	//	if(running)
	//		return;
	//	running = true;
	//	thread = new Thread(this);
	//	thread.start();
	//}
	
	//private synchronized void stop() {
	//	if(!running)
	//		return;
	//	running = false;
	//	try {
	//	thread.join();
	//	} catch(InterruptedException e) {
	//		e.printStackTrace();
	//	}
	//	System.exit(1);
	//}

	private void initComponents(){
		mainPanel = new javax.swing.JPanel();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
            .addGap(0, 50, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
		pack();
	}
	
	//public void run() {
	//	long lastTime = System.nanoTime();
	//	final double amountOfTicks = 60.0; //updating 60 times;
	//	double ns = 1000000000 / amountOfTicks;
	//	double delta = 0;
	//	while(running) {
			//this would be the game loop
	//		long now = System.nanoTime();
	//		delta +=(now - lastTime)/ns;
	//		lastTime = now;
	//		if(delta >= 1) {
	//			update();
	//			delta --;
	//		}
	//		render();
	//	}
	//	stop();
		
	//}
	
	public void update() {
        render();
        counter.setCurrentResult("Contagem de ovos: " + control.getCurrentPoints() + ".");
	}
	
	private void render() {
        platform.render(mainPanel);
    }
    
    public void startGame(){
        platform.game();
    }

    public void startGraphicGame(){
        mainPanel.removeAll();
        counter = new Counter("Hehehe");
        mainPanel.setSize(getSize());
        counter.setGoalDimensions(mainPanel.getWidth() - 400, mainPanel.getHeight()/2 - 400, 560, 100);
        mainPanel.setSize(getSize());
        counter.setcurrentResultsDimensions(mainPanel.getWidth() - 400, mainPanel.getHeight()/2 - 200, 560, 100);
        mainPanel.add(counter.getGoal());
        mainPanel.add(counter.getCurrentResult());
        platform = new Platform(control, mainPanel);
        render();
    }
    private Control control;
    private Platform platform;
    private Counter counter;
    private javax.swing.JPanel mainPanel;
}
