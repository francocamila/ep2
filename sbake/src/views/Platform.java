package views;

import static java.awt.event.KeyEvent.VK_DOWN;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_UP;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

//import javax.lang.model.util.ElementScanner6;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import control.Control;

public class Platform {

    public Platform(Control c, JPanel panel){
        control = c;
        rows = 10;
        columns = 10;
        //this.rows = control.quantityOfRows();
        //this.columns = control.quantityOfColumns();
        platform = new JButton [rows][columns];
        mainPanel = panel;
        initialize();
        futureMovement = VK_RIGHT;
} 

    public void initialize(){
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                platform[i][j] = new JButton();
                setProperties(platform[i][j], i, j);
                setListeners(platform[i][j]);
                if (i ==0 && j ==0){
                    platform[i][j].setBackground(Color.pink);
                    platform[i][j].requestFocus();
                    snake = new Snake(platform[i][j],10 * 10);
                }
                mainPanel.add(platform[i][j]);
            }
        }
        mainPanel.repaint();
    }
    public void setProperties(JButton jt, int x, int y){
        jt.setBounds(x*dimension, y*dimension, dimension, dimension);
        jt.setBackground(Color.darkGray);
        jt.setRequestFocusEnabled(false);
        if(control.hasFood(x,y)){
            jt.setIcon(new ImageIcon("src/gudetama_90.png"));
        }}

    public void setListeners(JButton jt){
        jt.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent ke){
                movements(ke.getKeyCode(), jt);
            }
        });
        }

    public boolean movements(int keyCode, JButton jt){
        JButton destiny = null;
        switch (keyCode){
            case VK_RIGHT:
            if(mainPanel.getComponentAt(jt.getX()+ dimension,jt.getY()) != null){
                if(mainPanel.getComponentAt(jt.getX() + dimension,jt.getY()) instanceof JButton){
                    destiny = (JButton) mainPanel.getComponentAt(jt.getX() + dimension,jt.getY());
                    destiny.requestFocus();
                    if(control.searchIfHasFood(destiny.getX()/dimension, destiny.getY()/dimension)){
                        snake.grow(new JButton());
                        control.setCurrentPoints(control.getCurrentPoints()+5);
                    }   
                    snake.movement(destiny);
                    futureMovement = VK_RIGHT;
                return true;
                }
                snake.setAlive(false);
                return false;
                }
                else{
                    snake.setAlive(false);
                    return false;
                }
            case VK_LEFT:
                if(mainPanel.getComponentAt(jt.getX() - dimension,jt.getY()) != null){
                    if(mainPanel.getComponentAt(jt.getX() - dimension,jt.getY()) instanceof JButton){
                        destiny = (JButton) mainPanel.getComponentAt(jt.getX() - dimension,jt.getY());
                        destiny.requestFocus();
                        if(control.searchIfHasFood(destiny.getX()/dimension, destiny.getY()/dimension)){
                            snake.grow(new JButton());
                            control.setCurrentPoints(control.getCurrentPoints()+5);
                        }
                        snake.movement(destiny);
                        futureMovement = VK_LEFT;
                    return true;
                    }
                snake.setAlive(false);    
                return false;
                }
                else{
                   snake.setAlive(false);
                   return false;
               }
            case VK_DOWN:
                if(mainPanel.getComponentAt(jt.getX(),jt.getY()+ dimension) != null){
                    if(mainPanel.getComponentAt(jt.getX(),jt.getY() + dimension) instanceof JButton){
                        destiny = (JButton) mainPanel.getComponentAt(jt.getX(),jt.getY() + dimension);
                        destiny.requestFocus();
                        if(control.searchIfHasFood(destiny.getX()/dimension, destiny.getY()/dimension)){
                            snake.grow(new JButton());
                            control.setCurrentPoints(control.getCurrentPoints()+5);
                        }
                        snake.movement(destiny);
                        futureMovement = VK_DOWN;
                    return true;
                    }
                snake.setAlive(false);    
                return false;
                }
                else{
                   snake.setAlive(false);
                   return false;
               }
            case VK_UP:
               if(mainPanel.getComponentAt(jt.getX(),jt.getY() - dimension) != null){
                    if(mainPanel.getComponentAt(jt.getX(),jt.getY() - dimension) instanceof JButton){
                        destiny = (JButton) mainPanel.getComponentAt(jt.getX(),jt.getY() - dimension);
                        destiny.requestFocus();
                        if(control.searchIfHasFood(destiny.getX()/dimension, destiny.getY()/dimension)){
                            snake.grow(new JButton());
                            control.setCurrentPoints(control.getCurrentPoints()+5);
                        }
                        snake.movement(destiny);
                        futureMovement = VK_UP;
                    return true;
                    }
                snake.setAlive(false);
                return false;
                }
                else{
                   snake.setAlive(false);
                   return false;
               }  
        }
        return false;
    }
    public void game(){
        //snake = new Snake(true);
        while(snake.isAlive()){
            if(control.getCurrentPoints() < control.getGoalPoints()){
                movements(futureMovement,snake.getHead());
                control.pauseGame(815);
            }
            else
                break;
        }
    }
    public void removeAll(){
        mainPanel.removeAll();
    }
    public void render(JPanel pan){
        for(int i = 0; i < 10; i++)
            for(int j = 0; j < 10; j++){
                if(control.hasFood(i,j))
                    platform[i][j].setIcon(new ImageIcon("src/gudetama_90.png"));
                else    
                    platform[i][j].setIcon(null);
        }
    }

    private static int dimension = 60;
    private Control control;
    private Snake snake;
    private Snake star;
    private JPanel mainPanel;
    private JButton [][] platform;
    private int futureMovement;
    private int rows;
    private int columns;
}