package views;

import java.awt.Color;

import javax.swing.JButton;

public class Snake{

    public Snake(JButton jt, int maximumLength){
        alive = true;
        head = jt;
        body = new JButton[maximumLength];
        currentBodySize = 0;
    }

    public void movement(JButton jt){
        if(jt.getBackground() == Color.darkGray){
            JButton aux = head;
            JButton previous = null;
            head = jt;
            head.setBackground(Color.pink);
            for (int i = 0; i < currentBodySize; i++){
                aux.setBackground(Color.pink);
                previous = body[i];
                body[i] = aux;
                body[i].setBackground(Color.pink);
                aux = previous;
            }
            aux.setBackground(Color.darkGray);
        }
        else
        setAlive(false);
    }

    public void grow(JButton jt){
        jt.setBackground(Color.pink);
        body[currentBodySize++] = jt;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive){
        this.alive = alive; 
    }

    public JButton getHead(){
        return head;
    }

    public void setHead(JButton head){
        this.head = head;
    }

    private JButton head;
    private JButton [] body;
    private int currentBodySize;
    private boolean alive; 
}