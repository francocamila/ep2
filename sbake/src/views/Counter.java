package views;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;

public class Counter {
    
    public Counter(String g){
        goal = new JLabel(g);
        currentResult = new JLabel("Ovos comidos:");
        goal.setFont(new Font("Arial", Font.PLAIN, 12));
        currentResult.setFont(new Font("Arial", Font.PLAIN, 12));
        currentResult.setForeground(Color.blue);
    }

    public JLabel getGoal() {
        return goal;
    }

    public void setGoal(String g) {
        goal.setText(g);
    }

    public JLabel getCurrentResult() {
        return currentResult;
    }

    public void setCurrentResult(String g) {
        currentResult.setText(g);
    }
    
    public void setGoalDimensions(int x,int y,int xd,int yd){
        goal.setBounds(x, y, xd, yd);
    }
    
    public void setcurrentResultsDimensions(int x,int y,int xd,int yd){
        currentResult.setBounds(x, y, xd, yd);
    }
    
    private JLabel goal;
    private JLabel currentResult;
}
