
package model;

import adapt.AdaptadorSubject;
import interfaces.Observer;

import java.util.Random;

public class Model {

    protected Model() {
        views = new AdaptadorSubject();
    }

    public static Model getInstance() {
        return (instance == null)?new Model():instance;
    }
    
    public void initGame(){
        Random gen = new Random();
        int x = gen.nextInt(10);
        x = (x == 0) ? 1 : x; //mais aleatoriedade
        int y = gen.nextInt(10);
        insertFood(x, y);
    }
    
    public void addView(Observer v){
        views.agregar(v);
    }
    
    public void update(){
        views.notificar();
    }
    
    public void createPlatform(int r,int c){
        platform = new Platform(r,c);
    }
    
    public void insertFood(int r, int c){
        platform.insert(r, c);
    }
    
    public void deleteFood(int r, int c){
        platform.delete(r, c);
    }
    
    public boolean hasFood(int r,int c){
        return platform.hasFood(r, c);
    }
    
    public boolean searchIfHasFood(int r,int c){
        if(hasFood(r,c)){
            deleteFood(r,c);
            Random gen = new Random();
            int x = gen.nextInt(10);
            int y = gen.nextInt(10);
            insertFood(x,y);
            update();
            return true;
        }
        update();
        return false;
    }
    
    public int quantityOfRows(){
        return 10;
    }
    
    public int quantityOfColumns(){
        return 10;
    }
    
    private Platform platform = null;
    private AdaptadorSubject views = null;
    private static Model instance = null;
}
